This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Smart Executor Bom

## [v3.3.0]

- Upgraded gcube-smartgears-bom to 2.5.1 [#27999]
- Updated maven-parent version to 1.2.0


## [v3.2.0] - 2023-09-08

- Upgraded gcube-smartgears-bom to 2.5.0


## [v3.1.0] - 2022-11-09

- Upgraded gcube-smartgears-bom to 2.2.0
- Added slf4j-api as provided [#23518]
- Added libs declared in gcube-smartgears-bom to set them as provided

## [v3.0.0] - 2021-06-16

- Switched smart-executor JSON management to gcube-jackson [#19647]


## [v2.0.0] - 2020-01-15

- Reorganized BOM


## [v1.0.0] [r4.17.0] - 2019-12-19

- First Release [#18180]

